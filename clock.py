#
# Copyright 2015, Catastrophe Corporation
# clock.py
# Author: Ashin Mandal (ashin.mandal@gmail.com)
# Version:  1.0
# Description: Python script for Twerp clock to tweet back to users
#

# Imports
import datetime
from time import sleep
import tweepy
from keys import *

# Initialisations
flag = 0
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token_key, access_token_secret)
api = tweepy.API(auth)

# Function to calculate Twerp Time
def getTwerpTime(ctime):
    numberOfNormalMicroSeconds = ((ctime.hour * 60 * 60 + ctime.minute * 60 + ctime.second) * 1000000) + ctime.microsecond
    numberOfTwerpSeconds = int((numberOfNormalMicroSeconds * ((43*67*89)/(24.0*60*60))) / 1000000)
    twerpHour = int(numberOfTwerpSeconds / (67*89))
    twerpMinute = int((numberOfTwerpSeconds - (twerpHour*67*89)) / 89)
    twerpSecond = numberOfTwerpSeconds - (twerpHour*67*89) - (twerpMinute*89)
    values = [twerpHour, twerpMinute, twerpSecond]
    joinValues = []
    for value in values:
        svalue = str(value)
        svalue = svalue if len(svalue) == 2 else "0" + svalue
        joinValues.append(svalue)
    return joinValues

def clock():
    while 1:
        try:
            # Get the last 20 mentions from @TheTwerpClock
            mentions = api.mentions_timeline()
            for mention in mentions:
                # mention.created_at gives the creation time in UTC
                mt = mention.created_at
                # Get UTC local time and remove Timezone information from it
                st = datetime.datetime.now(datetime.timezone.utc)
                st = st.replace(tzinfo=None)
                # Tweet back to the user if there is a mention within the last 60 seconds
                if (st-mt) < datetime.timedelta(seconds=60): 
                    print("Server Time (UTC): " + str(st))
                    print("Mention Creation Time (UTC): " + str(mt))
                    print("Time Difference: " + str((st-mt)))
                    # Set UTC Offset to 0 is it is None
                    if mention.user.utc_offset is None:
                        offset=0
                    else:
                        offset=mention.user.utc_offset
                    mt += datetime.timedelta(seconds=offset)
                    print("Mention Creation Time (Local): " + str(mt))
                    time = getTwerpTime(mt)
                    message = 'Hey @'+mention.user.screen_name+'! This is Twerp tweeting back to you with the correct time - '+':'.join(time)+'!'
                    api.update_status(status=message, in_reply_to_status_id=mention.id)
                    print('@'+str(mention.user.screen_name) +' tweeted within the last 60 seconds.')
                    print('Tweeting back with the time according to Twerp - ' + ':'.join(time) + '.')
                
            print ('Current time (according to Twerp): ' + ':'.join(getTwerpTime(datetime.datetime.now())))
            sleep(60)
        except (KeyboardInterrupt, IOError):
            break
clock()

