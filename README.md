# TwerpClock
Python code that enables Twerp Clock to tweet back to users on twitter.

NOTE: "keys.py" is not included in this repository and must be manually added and populated with the **consumer key** and **access token** for [**@TheTwerpClock**](https://twitter.com/TheTwerpClock) twitter account as variables for it's connection with the Twitter Apps API. The keys could also be declared as environment variables and imported in python.

### Variable Structure:
```
consumer_key = "XXXXXXXXXXXXXXX"
consumer_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
access_token = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
access_token_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

If they keys / access tokens are lost, go to https://apps.twitter.com/ and login with the respective bot credential to retrieve them.
